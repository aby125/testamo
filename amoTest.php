<?php
abstract class AbstractAmo
{
    abstract protected function getLeads();

    public $domain;
    
	public function __construct($domain, $login, $hash){
		$user=array(
  			'USER_LOGIN'=>$login,
			'USER_HASH'=>$hash
		);
		$this->domain=$domain;
		$link='https://'.$domain.'.amocrm.ru/private/api/auth.php?type=json';
		$curl=curl_init(); 
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_URL,$link);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
		curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($user));
		curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($curl,CURLOPT_HEADER,false);
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); 
		curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		$out=curl_exec($curl);
	}
}
class amoTest extends AbstractAmo {
	public function getleads() {
		$link='https://'.$this->domain.'.amocrm.ru/api/v2/leads';;
		$curl=curl_init(); 
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
		curl_setopt($curl,CURLOPT_URL,$link);
		curl_setopt($curl,CURLOPT_HEADER,false);
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); 
		curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($curl,CURLOPT_HTTPHEADER,array('IF-MODIFIED-SINCE: Mon, 01 Aug 2013 07:07:23'));
		$out=curl_exec($curl);
		$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
		curl_close($curl);
		return json_decode($out, false);
	}
}
	$domain = 'ssvtest';
	$login = 'ssv_test@winwin-tech.ru';
	$hash = 'ef4fe857c32021410df31dcc890001833eac4820';
	$test = new amoTest($domain, $login, $hash);
	print_r(json_encode($test->getLeads()->_embedded->items));
?>